ARG dvectorize_version
FROM dap_dvectorize:$dvectorize_version
RUN python3 -m pip --no-cache-dir install --upgrade \
        spectralcluster==0.0.7 \
        && \
mkdir /root/diarize
COPY . /root/diarize
