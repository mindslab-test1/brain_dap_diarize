import sys
import grpc
import argparse
import google.protobuf.empty_pb2 as empty

sys.path.append('/root/dvectorize')
from dap_pb2_grpc import DiarizeStub
from dap_pb2 import EmbList, WavBinary


class DiarizeClient(object):
    def __init__(self, remote='127.0.0.1:42001', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = DiarizeStub(channel)
        self.chunk_size = chunk_size

    def get_diarize(self, emblist):
        return self.stub.GetDiarization(emblist)

    def get_diarize_from_wav(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.GetDiarizationFromWav(wav_binary)

    def get_emb_config(self):
        return self.stub.GetEmbConfig(empty.Empty())

    def get_diarize_config(self):
        return self.stub.GetDiarizeConfig(empty.Empty())

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavBinary(bin=wav_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Diarize client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:42001',
                        help='grpc: ip:port')
    args = parser.parse_args()

    client = DiarizeClient(remote=args.remote)

    dvec_config = client.get_emb_config()
    diarize_config = client.get_diarize_config()
    print(dvec_config)
    print(diarize_config)

    emblist = [0.1 for x in range(dvec_config.emb_dim * 20)]
    emblist = EmbList(data=emblist)
    diarize_result = client.get_diarize(emblist).data

    print(diarize_result)

    with open('sample.wav', 'rb') as rf:
        diarize_result = client.get_diarize_from_wav(rf.read()).data
    print(diarize_result)
