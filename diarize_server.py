import os
import sys
import time
import grpc
import logging
import argparse
from concurrent import futures

from inference import DiarizeInference

sys.path.append('/root/dvectorize')
from dap_pb2_grpc import add_DiarizeServicer_to_server

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Diarize inference executor")
    parser.add_argument('-c', '--config',
                        nargs='?',
                        dest='config',
                        default='config/default.yaml',
                        help='Diarization config yaml file.',
                        type=str)
    parser.add_argument('-r', '--dvectorize-remote',
                        nargs='?',
                        dest='dvectorize_remote',
                        required=True,
                        help='dvectorize remote URL',
                        type=str)
    parser.add_argument('-l', '--log-level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=42001)
    parser.add_argument('--mins',
                        nargs='?',
                        dest='min_speakers',
                        help='minimum number of speakers',
                        type=int,
                        default=None)
    parser.add_argument('--maxs',
                        nargs='?',
                        dest='max_speakers',
                        help='maximum number of speakers',
                        type=int,
                        default=None)
    
    args = parser.parse_args()

    diarize = DiarizeInference(args)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1),)
    add_DiarizeServicer_to_server(diarize, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('diarize starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
